package com.example.gameapi.service;

import com.example.gameapi.entity.GameArtifact;
import com.example.gameapi.entity.Money;
import com.example.gameapi.entity.UseArtifact;
import com.example.gameapi.enums.Rating;
import com.example.gameapi.model.GameArtifactCreateRequest;
import com.example.gameapi.repository.GameArtifactRepository;
import com.example.gameapi.repository.MoneyRepository;
import com.example.gameapi.repository.UseArtifactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InitDataService {
    private final MoneyRepository moneyRepository;
    private final GameArtifactRepository gameArtifactRepository;
    private final UseArtifactRepository useArtifactRepository;

    /**
     * 첫 돈을 세팅한다. (0원으로..)
     */
    public void setFirstMoney() {
        Optional<Money> originData = moneyRepository.findById(1L); // 1번 id를 가진 데이터를 가져온다.. Optional이어서 있어도 가져오고 없어도 일단 가져온다.

        if (originData.isEmpty()) { // 만약 originData가 없으면....
            Money addData = new Money.MoneyBuilder().build(); // Money 기본값을 세팅하고
            moneyRepository.save(addData); // DB에 저장한다... 그러면 1번으로 0원짜리가 들어가겠지...
        }
    }

    /**
     * 게임 유물 정보들을 세팅한다.
     */
    public void setFirstArtifact() {
        List<GameArtifact> originList = gameArtifactRepository.findAll(); // 등록된 유물 리스트를 가져온다.

        if (originList.size() == 0) { // 등록된 유물 갯수가 하나도 없으면...
            List<GameArtifactCreateRequest> result = new LinkedList<>(); // 보석 정보들이 들어갈 빈 리스트 생성
            GameArtifactCreateRequest request1 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.NORMAL, "normal1", "normal1.jpg", 12D, 0D).build();
            result.add(request1);
            GameArtifactCreateRequest request2 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.NORMAL, "normal2", "normal2.jpg", 12D, 0D).build();
            result.add(request2);
            GameArtifactCreateRequest request3 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.NORMAL, "normal3", "normal3.jpg", 12D, 0D).build();
            result.add(request3);
            GameArtifactCreateRequest request4 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.NORMAL, "normal4", "normal4.jpg", 12D, 0D).build();
            result.add(request4);
            GameArtifactCreateRequest request5 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.NORMAL, "normal5", "normal5.jpg", 12D, 0D).build();
            result.add(request5);
            GameArtifactCreateRequest request6 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.NORMAL, "normal6", "normal6.jpg", 12D, 0D).build();
            result.add(request6);
            GameArtifactCreateRequest request7 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.RARE, "rare1", "rare1.jpg", 6D, 25D).build();
            result.add(request7);
            GameArtifactCreateRequest request8 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.RARE, "rare2", "rare2.jpg", 6D, 25D).build();
            result.add(request8);
            GameArtifactCreateRequest request9 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.RARE, "rare3", "rare3.jpg", 6D, 25D).build();
            result.add(request9);
            GameArtifactCreateRequest request10 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.UNIQUE, "emerald", "emerald.jpg", 4.5D, 10D).build();
            result.add(request10);
            GameArtifactCreateRequest request11 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.UNIQUE, "sapphire", "sapphire.jpg", 4.5D, 10D).build();
            result.add(request11);
            GameArtifactCreateRequest request12 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder
                    (Rating.LEGENDARY, "diamond", "diamond.jpg", 1D, 5D).build();
            result.add(request12);

            result.forEach(item -> {
                GameArtifact addData = new GameArtifact.GameArtifactBuilder(item).build();
                gameArtifactRepository.save(addData);
            });
        }
    }

    public void setFirstUseArtifact() {
        List<UseArtifact> useArtifacts = useArtifactRepository.findAll(); // db에서 등록되어있는 모든 보유유물들을 가져온다.

        if (useArtifacts.size() == 0) { // 보유유물 데이터가 하나도 없으면..
            List<GameArtifact> gameArtifacts = gameArtifactRepository.findAll(); // 등록된 유물 리스트를 불러온다.

            gameArtifacts.forEach(item -> { // 유물리스트에서 유물을 하나씩 던져주면서
                UseArtifact addData = new UseArtifact.UseArtifactBuilder(item).build(); // 보유유물 임시 데이터로 만들고
                useArtifactRepository.save(addData); // DB에 저장한다.
            });
        }
    }
}
