package com.example.gameapi.controller;

import com.example.gameapi.model.ChooseArtifactResponse;
import com.example.gameapi.model.SingleResult;
import com.example.gameapi.service.ArtifactChooseService;
import com.example.gameapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "보석 채굴")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/choose-artifact")
public class ChooseArtifactController {
    private final ArtifactChooseService artifactChooseService;

    @ApiOperation(value = "일반 채굴")
    @PostMapping("/silver")
    public SingleResult<ChooseArtifactResponse> chooseSilverBox() {
        return ResponseService.getSingleResult(artifactChooseService.getResult(false));
    }

    @ApiOperation(value = "특별 채굴")
    @PostMapping("/gold")
    public SingleResult<ChooseArtifactResponse> chooseGoldBox() {
        return ResponseService.getSingleResult(artifactChooseService.getResult(true));
    }
}
