package com.example.gameapi.repository;

import com.example.gameapi.entity.GameArtifact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameArtifactRepository extends JpaRepository<GameArtifact, Long> {
}
