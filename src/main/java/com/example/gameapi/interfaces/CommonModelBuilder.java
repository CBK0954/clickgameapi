package com.example.gameapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
