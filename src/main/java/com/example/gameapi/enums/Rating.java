package com.example.gameapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Rating {
    NORMAL("일반"),
    RARE("레어"),
    UNIQUE("유니크"),
    LEGENDARY("레전더리");

    private final String name;
}
