package com.example.gameapi.exception;

public class CNotEnoughMoneyException extends RuntimeException {
    public CNotEnoughMoneyException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotEnoughMoneyException(String msg) {
        super(msg);
    }

    public CNotEnoughMoneyException() {
        super();
    }
}
